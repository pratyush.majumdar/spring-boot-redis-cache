package com.pratyush.redis.cache.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pratyush.redis.cache.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
