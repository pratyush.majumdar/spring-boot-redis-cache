package com.pratyush.redis.cache.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pratyush.redis.cache.entity.User;
import com.pratyush.redis.cache.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	private static final String REDIS_CACHE_KEY = "users";
	
	@GetMapping(value = "/users")
	@Cacheable(REDIS_CACHE_KEY)
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
	
	@GetMapping(value = "/user/{id}")
	@Cacheable(value = REDIS_CACHE_KEY, key = "#id")
	public Optional<User> getUser(@PathVariable Long id) {
		return userService.getUser(id);
	}
	
	@PostMapping(value = "/user")
//	@CachePut(value = REDIS_CACHE_KEY)
	public ResponseEntity<User> addUser(@RequestBody User user) {
		try {
			userService.addUser(user);
			return new ResponseEntity<User>(user, HttpStatus.CREATED);
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(value = "/user/{id}")
	@CacheEvict(value = REDIS_CACHE_KEY, key = "#id")
	public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable Long id) {
		try {
			userService.updateUser(user);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}catch (NoSuchElementException e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value = "/user/{id}")
	@CacheEvict(value = REDIS_CACHE_KEY, key = "#id")
	public void deleteUser(@PathVariable Long id) {
		userService.deleteUser(id);
	}
}
