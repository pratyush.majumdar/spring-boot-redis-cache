# Spring Boot Redis Cache Implementation
Spring Data Redis provides easy configuration and access to Redis from Spring applications. It offers both low-level and high-level abstractions for interacting with the store, freeing the user from infrastructural concerns.

## Technologies Used
1. Spring Data JPA
2. Embedded H2 Database (Can be replaced by MySQL)
3. Spring Data Redis
4. Lombok

## Maven dependencies used
```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<dependency>
	<groupId>redis.clients</groupId>
	<artifactId>jedis</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
	<groupId>com.h2database</groupId>
	<artifactId>h2</artifactId>
	<scope>runtime</scope>
</dependency>
```

## Redis Download Location
[https://redis.io/download](https://redis.io/download)

## Rest APIs
- GET request to http://localhost:8080/users to get all users
- GET request to http://localhost:8080/user/1 to get specific user
- POST request to http://localhost:8080/user to add a user
- PUT request to http://localhost:8080/user/	1 to edit the user
- DELETE request to http://localhost:8080/user/1 to delete the user

```json
{
	"id" : 1,
	"firstName" : "John",
	"lastName" : "Doe",
	"emailId" : "john@domain.com",
	"age" : 35
}
```

### Build the Project skipping Tests
```mvn
mvn clean install -DskipTests
```

### Run the Project
```mvn
java -jar spring-boot-redis-cache-0.0.1-SNAPSHOT.jar
```